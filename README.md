## Documentación

- Instalar Node v16.x.x  (https://nodejs.org/dist/v16.17.0/node-v16.17.0-x64.msi)
- Clonar repositorio (https://gitlab.com/djorg90/tic-tac-toe)
- Instalar yarn v1.22.x (https://classic.yarnpkg.com/en/docs/install#windows-stable) o a traves de npm:

    ```bash
        npm install --global yarn
    ```

- Crear fichero .env en la raíz del proyecto con la siguiente variable de entorno:
    ```bash
        NEXT_PUBLIC_BACKEND_URL=http://localhost:3000
    ```
- Levantar el proyecto 
 - deveploment
    ```bash
        yarn dev
    ```
 - production
    ```bash
        yarn build
        yarn start
    ```   

Abrir [http://localhost:3000](http://localhost:3000) en el navegador para cargar el juego.

## Bot

Verifica si hay posiciones vacías en el tablero para jugar, en caso de que exista alguna posición disponible entonces busca cual es la posición ideal para ganar, si no tiene posibilidades de ganar aún, entonces busca la posición ideal para no perder, si no tiene posibilidades de perder aún, entonces elije una posición aleatoria dentro de las posiciones vacías. En caso de que no existan posiciones vacías para jugar o se detecte que el usuario o el bot ganó, entonces se da por finalizada la partida.

La posición ideal se obtiene a partir de buscar entre las posiciones vacías, las posiciones adyacentes a ella y verificar si el mismo jugador que se esta analizando (usuario o bot) ha jugado en ellas.
Ej: Posición 0 (P0) está vacía, existen 3 combinaciones posibles para ganar o perder desde esa posición, que son P1-P2, P3-P6, P4-P8.

![Image text](https://gitlab.com/djorg90/tic-tac-toe/-/raw/master/public/Diagramas%20de%20flujo.png)