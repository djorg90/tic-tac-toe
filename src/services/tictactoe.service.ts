import { rpost } from 'src/lib/request';
import {Match} from 'src/models/Match';

export const updateBoard = async (values: Match | {}) => {
    const { data } = await rpost('api/tic-tac-toe/play', values);
    return data;
};

export const updo = async (values: Match | {}) => {
    const { data } = await rpost('api/tic-tac-toe/undo', values);
    return data;
};
