import { useContext, createContext, useState } from 'react';
import {rpost} from 'src/lib/request';
import {Match} from 'src/models/Match';
import {ErrorMatch} from 'src/models/ErrorMatch';
import {updateBoard, updo} from 'src/services/tictactoe.service';

const initialData: Match = {
    matchId: "",
    boardState: [],
    nextMovement: {value: -1, character: '-'},
    history: [],
    userCharacter: "",
    isFinalized: false,
    winner: "",
}

const initialError: ErrorMatch = {
    hasErrors: false,
    errors: []
}

const TicTacToeContext = createContext({
    currentMatch: initialData,
    error: initialError,
    update: (value: Match | {}) => Promise.resolve(),
    isStarted: () => false as boolean,
    getValue: (value: number) => new String(),
    characterSelected: "",
    handleCharacter: (value: string)=>{},
    undoLastMove: () => {}
});

const TicTacToeProvider = ({ children }: any) => {

    const [currentMatch, setCurrentMatch] = useState<Match>(initialData);
    
    const [error, setError] = useState<ErrorMatch>(initialError);

    const[characterSelected, setCharacterSelected] = useState<string>("");
 
    const update = async (values: Match | {}) => {
        try {
            const data = await updateBoard(values);
            setCurrentMatch(data);
        } catch (error: any) {
            setError(error.response.data);   
        }
    }
    
    const handleCharacter = (value: string) => setCharacterSelected(value);

    const isStarted = (): boolean => currentMatch.matchId ? true : false;

    const getValue = (value : number) : string => {
        return isStarted() ? (currentMatch.boardState[value]?.character!=="-" ? currentMatch.boardState[value]?.character : ""):"";
    }

    const undoLastMove = async () => {
        try {
            const data = await updo(currentMatch);
            setCurrentMatch(data);
        } catch (error: any) {
            setError(error.response.data);   
        }
    }

    return (
        <TicTacToeContext.Provider
            value={{
                currentMatch,
                error,
                update,
                isStarted,
                getValue,
                characterSelected,
                handleCharacter,
                undoLastMove
            }}
        >
            {children}
        </TicTacToeContext.Provider>
    );
}

const useBoard = () => useContext(TicTacToeContext);

export { useBoard };

export default TicTacToeProvider;