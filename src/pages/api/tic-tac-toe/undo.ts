import { Match, Position } from 'src/models/Match';


export default function handler(req: any, res: any) {
    let result: Match = req.body;
    let penultPos: Position = result.history[result.history.length-2];
    let lastPos: Position = result.history[result.history.length-1];
    result.history.pop();
    result.history.pop();
    let nextMove: Position = {value: -1, character: '-'};
    if(result.history.length>=0){
        nextMove = result.history[result.history.length-1];
    }
    result.boardState[lastPos.value] = {value: lastPos.value, character: '-'};
    result.boardState[penultPos.value] = {value: penultPos.value, character: '-'};

    res.status(200).json(result);
}