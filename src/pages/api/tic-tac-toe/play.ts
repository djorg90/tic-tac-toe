import { Match, Position } from 'src/models/Match';
import { v4 as uuidv4 } from 'uuid';
import { setCookie, getCookie } from 'cookies-next';
import { ErrorMatch } from 'src/models/ErrorMatch';
import { ErrorType } from 'src/utils/errors_message';

export default function handler(req: any, res: any) {
    let result: Match;
    if (Object.keys(req.body).length === 0) {
        let matchId = uuidv4();
        setCookie('matchId', matchId, { req, res, maxAge: 60 * 60 * 24 });
        let match: Match = {
            boardState: fillBoard(),
            history: [],
            isFinalized: false,
            matchId: matchId,
            nextMovement: {value: -1, character: '-'},
            userCharacter: "",
            winner: ""
        }
        result = match;
        res.status(200).json(result)
    }
    else {
        if (isValidMatchId(req, res)) {
            let validate = validateMatch(req.body)
            if (validate.hasErrors) {
                let error: ErrorMatch = {hasErrors: false, errors: []};
                error.errors = validate.errors;
                error.hasErrors = validate.hasErrors;
                res.status(500).json(error);
            }
            else {
                result = req.body;
                result.boardState[result.nextMovement.value] =
                    { value: result.nextMovement.value, character: result.nextMovement.character };
                result.userCharacter = result.nextMovement.character;
                result.history.push(result.nextMovement);
                let validateWinner;
                validateWinner = isFinalized(result.boardState);
                if (validateWinner.isFinalize) {
                    result.isFinalized = true;
                    result.winner = validateWinner.winner;
                }
                else {
                    if (getAvailablePositions(result).length > 0) {
                        let bot: Position = getBotValue(result);
                        result.boardState[bot.value] =
                            { value: bot.value, character: bot.character };
                        validateWinner = isFinalized(result.boardState);
                        result.history.push(bot);
                        if (validateWinner.isFinalize) {
                            result.isFinalized = true;
                            result.winner = validateWinner.winner;
                        }
                    }
                    else {
                        result.isFinalized = true;
                        result.winner = "t";  //empatado
                    }
                }
                res.status(200).json(result)
            }
        }
        else {
            let error: ErrorMatch = {hasErrors: false, errors: []};
            error.errors = [ErrorType.NOT_VALID_MATCHID];
            error.hasErrors = true;
            res.status(500).json(error);
        }

    }
}

function fillBoard() {
    let board: Position[] = [];
    for (let index = 0; index <= 8; index++) {
        board.push({ value: index, character: '-' })
    }
    return board;
}

const getBotValue = (board: Match): Position => {

    let result: Position = { character: '-', value: 0 };
    let opponent: string = board.userCharacter === 'x' ? 'o' : 'x';
    let availables = getAvailablePositions(board);
    let forWin = getIdealPosition(availables, board.boardState, opponent, opponent);
    if (forWin === null) {
        let forNotToLost = getIdealPosition(availables, board.boardState, board.userCharacter, opponent);
        if (forNotToLost === null) {
            let index = Math.floor(Math.random() * availables.length) // random position
            result = { value: availables[index].value, character: opponent };
        }
        else {
            result = forNotToLost;
        }
    }
    else {
        result = forWin;
    }

    return result;
}


function getAvailablePositions(board: Match) {
    let available: Position[] = [];
    board.boardState.forEach(item => {
        if (item.character === "-") {
            available.push(item);
        }
    });
    return available;
}


function getIdealPosition(positions: Position[], state: Position[], charCompare: string, player: string) {
    let pos: Position = { value: -1, character: '-' };
    positions.forEach(item => {
        switch (item.value) {
            case 0:
                if ((state[1].character === charCompare && state[2].character === charCompare) ||
                    (state[4].character === charCompare && state[8].character === charCompare) ||
                    (state[3].character === charCompare && state[6].character === charCompare)) {
                    pos = { value: item.value, character: player };
                }
                break;
            case 1:
                if ((state[0].character === charCompare && state[2].character === charCompare) ||
                    (state[4].character === charCompare && state[7].character === charCompare)) {
                    pos = { value: item.value, character: player };
                }
                break;
            case 2:
                if ((state[0].character === charCompare && state[1].character === charCompare) ||
                    (state[4].character === charCompare && state[6].character === charCompare) ||
                    (state[5].character === charCompare && state[8].character === charCompare)) {
                    pos = { value: item.value, character: player };
                }
                break;
            case 3:
                if ((state[0].character === charCompare && state[6].character === charCompare) ||
                    (state[4].character === charCompare && state[5].character === charCompare)) {
                    pos = { value: item.value, character: player };
                }
                break;
            case 4:
                if ((state[0].character === charCompare && state[8].character === charCompare) ||
                    (state[2].character === charCompare && state[6].character === charCompare) ||
                    (state[1].character === charCompare && state[7].character === charCompare) ||
                    (state[3].character === charCompare && state[5].character === charCompare)) {
                    pos = { value: item.value, character: player };
                }
                break;
            case 5:
                if ((state[2].character === charCompare && state[8].character === charCompare) ||
                    (state[3].character === charCompare && state[4].character === charCompare)) {
                    pos = { value: item.value, character: player };
                }
                break;
            case 6:
                if ((state[0].character === charCompare && state[3].character === charCompare) ||
                    (state[4].character === charCompare && state[2].character === charCompare) ||
                    (state[7].character === charCompare && state[8].character === charCompare)) {
                    pos = { value: item.value, character: player };
                }
                break;
            case 7:
                if ((state[6].character === charCompare && state[8].character === charCompare) ||
                    (state[1].character === charCompare && state[4].character === charCompare)) {
                    pos = { value: item.value, character: player };
                }
                break;
            case 8:
                if ((state[0].character === charCompare && state[4].character === charCompare) ||
                    (state[2].character === charCompare && state[5].character === charCompare) ||
                    (state[6].character === charCompare && state[7].character === charCompare)) {
                    pos = { value: item.value, character: player };
                }
                break;
            default:
                break;
        }
    });

    return pos.value === -1 ? null : pos;
}


function isFinalized(boardState: Position[]) {
    let result = { isFinalize: false, winner: "" }
    if (boardState[0].character === boardState[1].character && boardState[0].character === boardState[2].character && boardState[0].character !== "-") {
        result = { isFinalize: true, winner: boardState[0].character };
    }
    if (boardState[3].character === boardState[4].character && boardState[3].character === boardState[5].character && boardState[3].character !== "-") {
        result = { isFinalize: true, winner: boardState[3].character };
    }
    if (boardState[6].character === boardState[7].character && boardState[6].character === boardState[8].character && boardState[6].character !== "-") {
        result = { isFinalize: true, winner: boardState[6].character };
    }
    if (boardState[0].character === boardState[3].character && boardState[0].character === boardState[6].character && boardState[0].character !== "-") {
        result = { isFinalize: true, winner: boardState[0].character };
    }
    if (boardState[1].character === boardState[4].character && boardState[1].character === boardState[7].character && boardState[1].character !== "-") {
        result = { isFinalize: true, winner: boardState[1].character };
    }
    if (boardState[2].character === boardState[5].character && boardState[2].character === boardState[8].character && boardState[2].character !== "-") {
        result = { isFinalize: true, winner: boardState[2].character };
    }
    if (boardState[0].character === boardState[4].character && boardState[0].character === boardState[8].character && boardState[0].character !== "-") {
        result = { isFinalize: true, winner: boardState[0].character };
    }
    if (boardState[2].character === boardState[4].character && boardState[2].character === boardState[6].character && boardState[2].character !== "-") {
        result = { isFinalize: true, winner: boardState[2].character };
    }

    return result;
}

function validateMatch(match: Match): ErrorMatch {
    let result = false;
    let errors: Array<string> = [];
    if (!validatePayload(match)) {
        errors.push(ErrorType.NOT_VALID_PAYLOAD_FORMAT);
        result = true;
    }
    if (!validateMove(match.nextMovement)) {
        errors.push(ErrorType.NOT_VALID_MOVE);
        result = true;
    }
    if(match.isFinalized){
        errors.push(ErrorType.MATCH_HAS_FINISHED);
        result = true;
    }

    return { errors: errors, hasErrors: result }
}

function isValidMatchId(req: any, res: any) {
    return req.body.matchId === getCookie('matchId', { req, res });
}

function validatePayload(match: Match){
    let props =  Object.keys(match);

    return props.length === 7 && 
     props.includes('matchId') && props.includes('boardState') && 
     props.includes('nextMovement') && props.includes('history') && 
     props.includes('userCharacter') && props.includes('isFinalized') && 
     props.includes('winner');
}

function validateMove(move: Position){
    let props =  Object.keys(move);
    return props.length === 2 && props.includes('value') && 
    props.includes('character') && move.value >= 0 && move.value <=8 && 
    (move.character === 'x' || move.character === 'o')
}