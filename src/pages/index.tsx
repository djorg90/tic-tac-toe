import type { NextPage } from 'next';
import Board from 'src/components/Board';
import TicTacToeProvider from 'src/providers/TicTacToeProvider';

const Home: NextPage = () => {

  return (
    <TicTacToeProvider>
      <Board />
    </TicTacToeProvider>
  );
};

export default Home;