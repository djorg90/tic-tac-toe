import React, { useState, useCallback } from 'react';
import { Grid, ButtonGroup, Button, Alert, Typography } from '@mui/material';
import UndoIcon from '@mui/icons-material/Undo';
import Cell from './Cell';
import Value from './Value';
import { useBoard } from 'src/providers/TicTacToeProvider';

const Board = () => {

    const { currentMatch, error, isStarted, getValue, update, characterSelected, handleCharacter, undoLastMove } = useBoard();

    const startMatch = () => {
        update({});
    }

    return (
        <Grid container sx={{ cursor: isStarted() && !currentMatch.isFinalized ? 'pointer' : 'not-allowed' }} justifyContent={'center'}>
            <Grid item xs={2}>
                <ButtonGroup color="primary" disabled={currentMatch.isFinalized ? false : isStarted()}>
                    <Button
                        onClick={() => handleCharacter('x')}
                        variant={characterSelected === 'x' ? 'contained' : 'outlined'}
                    >
                        {'x'}
                    </Button>
                    <Button
                        onClick={() => handleCharacter('o')}
                        variant={characterSelected === 'o' ? 'contained' : 'outlined'}
                    >
                        {'o'}
                    </Button>
                </ButtonGroup>
            </Grid>
            <Grid item xs={4}>
                { currentMatch.winner && currentMatch.winner !== "t" && currentMatch.winner !== currentMatch.userCharacter && <Alert severity="error">{'¡Has perdido!'}</Alert> }
                { currentMatch.winner && currentMatch.winner === currentMatch.userCharacter && <Alert severity="success">{'¡Has Ganado!'}</Alert> }
                { currentMatch.winner && currentMatch.winner === "t" && <Alert severity="warning">{'¡Empatado!'}</Alert> }
            </Grid>
            <Grid item xs={12}>
                {
                    error.hasErrors && 
                    error.errors.map(e => <Typography color={'red'} key={e}>{e}</Typography>)
                }
            </Grid>
            <Grid item xs={10} mt={3}>
                <Grid container justifyContent={'center'}>
                    <Cell position={0} border={{ r: 1 }}><Value character={getValue(0)} /></Cell>
                    <Cell position={1} border={{ r: 1 }}><Value character={getValue(1)} /></Cell>
                    <Cell position={2}><Value character={getValue(2)} /></Cell>
                </Grid>
            </Grid>
            <Grid item xs={10}>
                <Grid container justifyContent={'center'}>
                    <Cell position={3} border={{ t: 1, b: 1, r: 1 }}><Value character={getValue(3)} /></Cell>
                    <Cell position={4} border={{ t: 1, b: 1, r: 1 }}><Value character={getValue(4)} /></Cell>
                    <Cell position={5} border={{ t: 1, b: 1 }}><Value character={getValue(5)} /></Cell>
                </Grid>
            </Grid>
            <Grid item xs={10}>
                <Grid container justifyContent={'center'}>
                    <Cell position={6} border={{ r: 1 }}><Value character={getValue(6)} /></Cell>
                    <Cell position={7} border={{ r: 1 }}><Value character={getValue(7)} /></Cell>
                    <Cell position={8}><Value character={getValue(8)} /></Cell>
                </Grid>
            </Grid>
            <Grid item xs={12}>
                <Grid container mt={5} justifyContent={'center'}>
                    <Button
                        onClick={startMatch}
                        disabled={currentMatch.isFinalized ? false : (!characterSelected || isStarted())}
                        variant={'contained'}
                    >
                        {'Nueva partida'}
                    </Button>
                    <Button
                        startIcon={<UndoIcon />}
                        sx={{ml:4}}
                        onClick={undoLastMove}
                        disabled={currentMatch.isFinalized ? true : currentMatch.history.length===0}
                        variant={'contained'}
                    >
                        {'Deshacer'}
                    </Button>
                </Grid>
            </Grid>
        </Grid >
    );
};

export default Board;