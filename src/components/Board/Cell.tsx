import React, { Component } from 'react';
import { Grid } from '@mui/material';
import { useBoard } from 'src/providers/TicTacToeProvider';


interface CellProps {
    children?: any;
    border?: { t?: number, b?: number, l?: number, r?: number };
    characterSelected?: string;
    position: number
}

const Cell = (props: CellProps) => {

    const { isStarted, currentMatch, update, characterSelected } = useBoard();

    const mark = () => {
        if (isStarted() && !currentMatch.isFinalized && currentMatch.boardState[props.position].character!=="x" && currentMatch.boardState[props.position].character!=="o") {
            let upd = currentMatch;
            upd.nextMovement = { value: props.position, character: characterSelected };
            update(upd);
        }
    } 

    return (
        <Grid
            item
            xs={2}
            p={5}
            textAlign={'center'}
            onClick={mark}
            sx={{ 
                borderLeft:  props.border ? props.border.l : 0, 
                borderRight:  props.border ? props.border.r : 0, 
                borderTop: props.border ? props.border.t : 0, 
                borderBottom: props.border ? props.border.b : 0
            }}>
            {props.children}
        </Grid>
    );
};

export default Cell;