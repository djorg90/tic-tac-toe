import React from 'react';
import { Typography } from '@mui/material';

interface ValueProps {
    character: String;
}

const Value = ({ character }: ValueProps) => {
    return (
            <Typography
             variant='h1' 
             sx={{fontSize: '30px', fontWeight: 600, color: character==="x" ? 'blue' : 'black'}}>
                {character}
            </Typography>
    );
};

export default Value;