
export interface Position{
    value: number;
    character: string | 'x' | 'o' | '-'
}

export interface Match {
    matchId: string;
    boardState: Position[];
    nextMovement: Position;
    history: Position[];
    userCharacter: string;
    isFinalized: boolean;
    winner: string;
}