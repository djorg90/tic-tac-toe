export interface ErrorMatch{
    errors: Array<string>;
    hasErrors: boolean;
}