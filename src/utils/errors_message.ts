export enum ErrorType{
  NOT_VALID_PAYLOAD_FORMAT = '_Not valid payload format_',
  NOT_VALID_MOVE = '_Not valid move_',
  NOT_VALID_MATCHID = '_Not valid matchId_',
  MATCH_HAS_FINISHED = '_Match has finished_'
}