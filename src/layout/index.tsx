import React from 'react';
import {CssBaseline, Box, Container} from '@mui/material';

interface LayoutProps{
    children: any;
}

const Layout = ({children} : LayoutProps) => {
  
    return (
        <Box sx={{ display: 'flex' }}>
          <CssBaseline />
            <Container maxWidth="lg" sx={{ mt: 4, mb: 4 }}>
              {children}
            </Container>
        </Box>
    );
};

export default Layout;